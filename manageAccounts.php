<?php
	include('connectionData.txt');
	$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
	or die('Error connecting to MySQL server.');
	$cust=$_POST['cust_id'];
	$query = "select * from accounts_new WHERE cust_id=?;";
	?>

<!DOCTYPE html>
<html>
<head>
  <title>Stock Sesh</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>


	.btn-space{
			padding-top:10px;
			padding-bottom: 10px;
			margin-bottom: 5px;
		}

	 h3{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
			color: grey;
		}

	 h2{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}

	 h1{
			font-weight:bold;
			font-variant: small-caps;
		}


		body{
			background-color: #eeeeee;
		}

	    #body-wrapper {
			background-color: #ffffff;
			margin-bottom: 0;
			margin: 0 auto;
			border-radius: 0; 
			width: 100%;
			position: auto;
			max-width: 1100px;
	    }



		footer{
			background-color: #ffffff;
      		margin-bottom: 0;
      		margin: 0 auto;
      		width: 100%;
      		position: auto;
      		max-width: 1100px;
			padding: 5px;
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}


		

  </style>
</head>
<body>


<br>
<div class="container" id="body-wrapper">
	<div class="container text-center">
		<div class="row content">
			<div class="col-sm-5 text-left">
				<h1><a href="home.php">Stock Sesh</a></h1>
				<h3>stock simulater</h3>
			</div>
			<div class="col-sm-5 text-right">
			<br>
			<br>

				<form action="tickerSearch.php" method="POST">
					<div class="form-inline form-space">
						<label for="ticker">Search stock ticker: </label>
						<input class="form-control" type="text" name="ticker">
						<input class="btn btn-success" type="submit" value="Submit">
						<input class="btn btn-danger" type="reset" value="Erase">
					</div>
				</form>

			</div>
		</div>
	</div>
	<hr>

			<div class="container text-center">
				<div class="row content">
					<div class="col-sm-3 text-left" id="div_links">
							<br>
							<a class="btn btn-info btn-space" href="addFundsHome.php"> Add funds to an account</a>
							<br>
							<a class="btn btn-info btn-space" href="newAccount.php"> Create a account</a>
							<br>
							<a class="btn btn-info btn-space" href="buyStocks.php"> Purchase stocks</a>
							<!--<br>
							<a class="btn btn-info btn-space" href="buyForex.php"> Purchase Foreign Exchange</a>
							<br>
							<a class="btn btn-info btn-space" href="buyCommodities.php"> Purchase Commodities</a>-->
							<br>
							<a class="btn btn-info btn-space" href="sellStocks.php"> Reset Account</a>
							<br>
							<a class="btn btn-info btn-space" href="simulate.php"> Simulate November Trading</a>
							<br>

						</div>
					    <div class="col-sm-5 text-center" id="div_top_accounts">
							<h2>Your account(s) info is below. </h2>
							<br>
							<br>
							<br>
							

							<?php
								if(!($stmt = mysqli_prepare($conn, $query))){
									print "Prepare Failed";
								}if(!(mysqli_stmt_bind_param($stmt,"s", $cust))){
									print "Binding parameters failed";
								}if(!(mysqli_stmt_execute($stmt))){
									print "Execution failed";
								}

			
								print "<pre>";
    							print "\n";
								printf("%-15s %+5s %-15s", "Account ID", "", "Balance($)");							
    							print "\n------------------------------------";
								$result = mysqli_stmt_get_result($stmt);
								while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
    									print "\n";
									
										printf("%-15s %+5s %-15s", $row[account_id], "$", $row[balance]);							
  									}

								print "</pre>";
								

								mysqli_free_result($result);
								mysqli_stmt_close($stmt);
								mysqli_close($conn);
							?>




						</div>
						<div class="col-sm-3">

						</div>

				</div>
		  </div>
<br>
  </div>

	<br>

	<!-- <footer>
		<div class="container" id="footer_container">
			<div class="col-sm-4">
				<h3> Contact </h3>
				<hr/>
				<b>email:</b> YourScrewed@canntbebothered.com
				<br/>
				<b>office:</b> Somewhere in the USA 
				<br/>
				<br/>
			<div/>
		</div>
	</footer> -->
	<br/>

</body>
</html>
