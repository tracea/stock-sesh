<?php
	include('connectionData.txt');
	$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
	or die('Error connecting to MySQL server.');

	$amount =$_POST['amount'];
	$account=$_POST['account_id'];
	$query = "UPDATE accounts_new SET balance = (balance + ?) where account_id=?;";
	?>

<html>
<head>
	<title>Stock Sesh</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>

	 h3{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
			color: grey;
		}

	 h2{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}

	 h1{
			font-weight:bold;
			font-variant: small-caps;
		}


		body{
			background-color: #eeeeee;
		}

	    #body-wrapper {
			background-color: #ffffff;
			margin-bottom: 0;
			margin: 0 auto;
			border-radius: 0; 
			width: 100%;
			position: auto;
			max-width: 1100px;
	    }



		footer{
			background-color: #ffffff;
      		margin-bottom: 0;
      		margin: 0 auto;
      		width: 100%;
      		position: auto;
      		max-width: 1100px;
			padding: 5px;
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}

		

  </style>
</head>



<body>


<br>
<div class="container" id="body-wrapper">
	<div class="container text-center">
		<div class="row content">
			<div class="col-sm-5 text-left">
  

	<p>
		<a href="home.php"><h1>Stock Sesh</h1></a>
		<h3>stock simulater</h3>
	</p>



			</div>
			<div class="col-sm-5 text-right">
			<br>
			<br>
			<br>
				<form action="tickerSearch.php" method="POST">
					<div class="form-inline form-space">
						<label for="ticker">Search stock ticker: </label>
						<input class="form-control" type="text" name="ticker">
						<input class="btn btn-success" type="submit" value="Submit">
						<input class="btn btn-danger" type="reset" value="Erase">
					</div>
				</form>

			</div>
		</div>
	</div>
	<hr>

			<div class="container text-center">
				<div class="row content">
					    <div class="col-sm-5 text-left" id="line_top_x">
							<?php
								if(!($stmt = mysqli_prepare($conn, $query))){
									print "Prepare Failed";
								}if(!(mysqli_stmt_bind_param($stmt,"ss", $amount, $account))){
									print "Binding parameters failed";
								}if(!(mysqli_stmt_execute($stmt))){
									print "Execution failed";
								}

								if(mysqli_stmt_affected_rows($stmt) > 0){ 
									printf("You have successfully add %s to account %s", $amount, $account);
								}else{
									print" Unable to add funds, not a valid account";
								}

								//mysqli_free_result($result);
								mysqli_stmt_close($stmt);
								mysqli_close($conn);
							?>

							<br>
							<br>

						</div>

				</div>
		  </div>
<br>
  </div>

	<br>

  <div ></div>
	<!-- <footer>
		<div class="container" id="footer_container">
			<div class="col-sm-4">
				<h3> Contact </h3>
				<hr/>
				<b>email:</b> YourScrewed@canntbebothered.com
				<br/>
				<b>office:</b> Somewhere in the USA 
				<br/>
				<br/>
			<div/>
		</div>
	</footer> -->
	<br/>

</body>
</html>
