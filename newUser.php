<!DOCTYPE html>
<html>
<head>
  <title>Stock Sesh</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>

	 .form-space{

			padding-top:10px;
			padding-bottom: 10px;
			margin-bottom: 5px;
		}



	 h3{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
			color: grey;
		}

	 h2{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}

	 h1{
			font-weight:bold;
			font-variant: small-caps;
		}


		body{
			background-color: #eeeeee;
		}

	    #body-wrapper {
			background-color: #ffffff;
			margin-bottom: 0;
			margin: 0 auto;
			border-radius: 0; 
			width: 100%;
			position: auto;
			max-width: 1100px;
	    }



		footer{
			background-color: #ffffff;
      		margin-bottom: 0;
      		margin: 0 auto;
      		width: 100%;
      		position: auto;
      		max-width: 1100px;
			padding: 5px;
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}


		

  </style>
</head>
<body>


<br>
<div class="container" id="body-wrapper">
	<div class="container text-center">
		<div class="row content">
			<div class="col-sm-5 text-left">
				<h1><a href="home.php">Stock Sesh</a></h1>
				<h3>stock simulater</h3>
			</div>
			<div class="col-sm-5 text-right">
			<br>
			<br>
				<form action="tickerSearch.php" method="POST">
					<div class="form-inline form-space">
						<label for="ticker">Search stock ticker: </label>
						<input class="form-control" type="text" name="ticker">
						<input class="btn btn-success" type="submit" value="Submit">
						<input class="btn btn-danger" type="reset" value="Erase">
					</div>
				</form>

			</div>
		</div>
	</div>
	<hr>

			<div class="container text-center">
				<div class="row content">
					    <div class="col-sm-8 text-left" id="div_top_accounts">

							<form action="customerAdd.php" method="POST">
								<div class="form-inline form-space">
									<label for="fname">First Name:</label>
    								<input type="text" class="form-control" name="fname">
									<label for="lname" >Last Name:</label>
    								<input type="text" class="form-control" name="lname">
  								</div>
								<div class="form-inline form-space">
									<label for="state">State:</label>
    								<input type="text" class="form-control" name="state">
									<label for="zipcode">zip code:</label>
    								<input type="text" class="form-control" name="zipcode">
  								</div>
								<div class="form-group">
									<label for="email">Email address:</label>
    								<input type="text" class="form-control" name="email">
  								</div>
  								<div class="form-group">
    								<label for="ssn">SSN:</label>
    								<input type="text" class="form-control" name="ssn">
  								</div>
  								<div class="form-group">
    								<label for="address">Address:</label>
    								<input type="text" class="form-control" name="address">
  								</div>
  								<input type="submit" class="btn btn-default" value="Submit">
							</form>

				    <!--<form action="customerAdd.php" method="POST">
					    First Name: <input type="text" name="fname"> 	<br/>
						<input type="submit" value="Submit">
					</form> -->

						</div>

				</div>
		  </div>
<br>
  </div>

	<br>

	<!-- <footer>
		<div class="container" id="footer_container">
			<div class="col-sm-4">
				<h3> Contact </h3>
				<hr/>
				<b>email:</b> YourScrewed@canntbebothered.com
				<br/>
				<b>office:</b> Somewhere in the USA 
				<br/>
				<br/>
			<div/>
		</div>
	</footer> -->
	<br/>

</body>
</html>
