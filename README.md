```
#!php


```
# Stock Sesh #
### The website for this project can be found [here](http://ix.cs.uoregon.edu/~tracea/stock-sesh/home.php) ###
### Guest account information - Port: 3225, Username: guest, Password: guest ###
## Table of Contents ##
1. Summary
2. Logical Design
3. Physical Design and Contents of Tables (mysqldump)
4. List of Applications
5. User's Guide
6. Implementation Code
7. Conclusion

##1. Summary ##

Our database is modeling data that would be used for an investment application. In our database, we will
store customer information such as customer id, name, address and social security number. Customers can
have multiple accounts consisting of an account id, type, balances, and positions; where a position stores the
price and date a customer bought or sold an investment and how many were bought or sold. An investment
can be a stock, commodity or foreign exchange. All three investment types have an investment_id, date, price while stocks have a ticker and commodities and foreign exchange have a name. The database will also
have brokers which are assigned to many accounts and consist of email, name,  social security
number and a commission rate.

Applications on this database consist of account operations for customers to create an account and choose
a broker, view the current value of an account, deposit or withdraw money from an account. An additional application will allow customers to open positions on a variety of
investments. Before investing we also have applications to allow you to search the price of a specific stock, were the customers will see a graph of how that stock has performed over the first 3 weeks of November. When customers buys stocks, they purchase stocks at the November 1st price, we then allow the customer to run a simulation on a specific account. The simulation will return a percentage of gains or loss of how that investments stocks performed from November 1st to November 21st. We also provide an application to reset an account, which will delete all positions held by that account and return the original amount invested on November 1st date to that accounts balance.

##2. Logical Design ##

![ER_diagram.PNG](https://bitbucket.org/repo/6E9bnn/images/1637224107-ER_diagram.PNG)

##3. Physical Design and Contents of Tables##
To view the mysql dump file click [here](https://bitbucket.org/tracea/stock-sesh/src/d9bc1e7e5316f1b6dad9517ef644b0526dc84055/sql_dump/?at=master)

##4. List of Applications ##
* Create User: Users input information and an entry in the customer table is created for the user. The user gets assigned a customer_id.

* Create Account: Users can create multiple accounts which are added into the account table. Each account has an associated account_id.

* Add Funds to An Account: Users can add the desired amount of money to an Account. This changes the balance field in the accounts table.

* Reset Account: This removes all the positions in the positions table for the account_id specified. This also restores the balance field in the account table to the original balance before any purchases were made.

* Search Ticker: This queries the stock table for all the price information for the specified ticker. The price information is displayed as a graph.

* Purchase Stocks: Users can buy a specified quantity of a stock using a specified account. If the balance of the account is sufficient for the purchase, the total price of the stock purchase is deducted from the balance field of accounts. A new entry in the positions table is created specifying which account the position is on, how much of the stock was purchased and what price it was purchased for. If there are insufficient funds in the account, a message is displayed to the user and there is no change to the database

* Simulate Trading for November: Users can simulate the performance of a specified account for the month of November. The results are displayed in a graph. If the simulation is the highest percentage gain for the account, the best_sim field for the account is updated.

##Applications of our database that do not take user input##

* On the home page a query is made to the accounts join customers_new tables to return the top 10 performing accounts. This requires an account to have ran the simulation to store its best simulation value in the accounts table.

* Also the manageAccountsHome page runs a query to display all current user and their customer id.

* After choosing an account from the manageAccountsHome page you will be redirected to the manageAccounts page where a query to the accounts table will present all the account id's and balances associated with that customer.

* The create account page will run a query to populate a drop down bar with all the names of all the current brokers.

##5. User's Guide ##

### Any time you would like to find pricing information on a stock, you can search for the stock by its ticker in the top right-hand corner. This will take you to a page with a graph of the stocks price for the month of Novemver. Also at anytime you can click blue Stock Sesh logo in the top left of any page to be redirected to the home page. ###
1. Click on the "Create a new user" button.
2. Fill in the fields of the form and click submit
3. Go back to the Home Page and click the "Manage Accounts Button"
4. A table listing all the customers and their customer_id's will be displayed. In the text box below the table, type in your customer number and click the "Submit" button.
5. A table listing all the customer's account_ids and balances is displayed as well as actions you can perform on the accounts. From here you can create new accounts, buy stocks, add funds to an account, reset an account and simulate trading for the month of November.

##6. Implementation Code ##

To view the Source Code, either click on the "Source" tab on the left or click [here](https://bitbucket.org/tracea/stock-sesh/src).
The simulate_trading.php and reset.php both contain the more complicated interesting queries and computations.


##7. Conclusion ##
### What have we done ####

* Search stocks which presents a graph of the stocks performance over the last 3 weeks.
* Create new users, adds a new user to data base.
* Each user can create multiple investment accounts, default of $10,000.
* In creating an account you need to pick a type and a broker from a drop down bar, the values of the drop down bar where the brokers drop down bar is populated by running a query on the brokers table.
* Each user can manipulate value of an account by adding money to the account at any given time.
* Each investment account can be used to purchase stocks by their tickers, purchase price is Nov 1st for all stocks. Each time a stock is purchased a new positions is created and added to the positions table, and cost of stocks is subtracted from balance of investment account.
* You can run stock simulation on all investment accounts, this compares the price of stock from Nov. 1st to Nov. 21st and returns the simulated gains your account made.
* The home page runs a query to update the current top 10 performing investment accounts.
* You can reset a investment account at any time, which deletes all positions, runs a query on the Nov 1st price to refund the amount of money you had invested on that investment account.

### What could we do with more time ###

* Buying commodities
* Buying currencies
* Simulate commodity positions and currency positions
* Streamline the process of signing up for the website
* Allow users to buy stocks on different days rather than only November 1st
* A Dynamic List of stocks to watch based on some sort of criteria like largest percentage increase/decrease
* Create a automation to get all the stock names for all the stocks in our table and create a table for stock ticker to stock name. The stock data we got only provided tickers for stocks not names, given the size of the data it was to much for us to fill in by hand.