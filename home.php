<!DOCTYPE html>
<html>
<head>
  <title>Stock Sesh</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>


	.btn-space{
			padding-top:10px;
			padding-bottom: 10px;
			margin-bottom: 5px;
		}

	 h3{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
			color: grey;
		}

	 h2{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}

	 h1{
			font-weight:bold;
			font-variant: small-caps;
		}


		body{
			background-color: #eeeeee;
		}

	    #body-wrapper {
			background-color: #ffffff;
			margin-bottom: 0;
			margin: 0 auto;
			border-radius: 0; 
			width: 100%;
			position: auto;
			max-width: 1100px;
	    }



		footer{
			background-color: #ffffff;
      		margin-bottom: 0;
      		margin: 0 auto;
      		width: 100%;
      		position: auto;
      		max-width: 1100px;
			padding: 5px;
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}


		

  </style>
</head>
<body>


<br>
<div class="container" id="body-wrapper">
	<div class="container text-center">
		<div class="row content">
			<div class="col-sm-5 text-left">
				<h1><a href="#">Stock Sesh</a></h1>
				<h3>stock simulater</h3>
			</div>
			<div class="col-sm-5 text-right">
			<br>
			<br>

				<form action="tickerSearch.php" method="POST">
					<div class="form-inline form-space">
						<label for="ticker">Search stock ticker: </label>
						<input class="form-control" type="text" name="ticker">
						<input class="btn btn-success" type="submit" value="Submit">
						<input class="btn btn-danger" type="reset" value="Erase">
					</div>
				</form>

			</div>
		</div>
	</div>
	<hr>

			<div class="container text-center">
				<div class="row content">
					<div class="col-sm-3 text-left" id="div_links">
							<br>
							<a class="btn btn-info btn-space" href="newUser.php"> Create a new user </a>
							<br>
							<a class="btn btn-info btn-space" href="manageAccountsHome.php">Account Management</a>
						<!--	
<a class="btn btn-info btn-space" href="addFunds.html"> Add funds to an account</a>
							<br>
							<a class="btn btn-info btn-space" href="newAccount.php"> Create a account</a>
							<br>
							<a class="btn btn-info btn-space" href="buyStocks.php"> Purchase stocks</a>
							<br>
							<a class="btn btn-info btn-space" href="simulate.php"> Simulate November Trading</a>
							<br>
							<a class="btn btn-info btn-space" href="sellStocks.php"> Sell positions</a>
						-->

						</div>
					    <div class="col-sm-5 text-left" id="div_top_accounts">
							Top portfolios of November
							<?php
								include('connectionData.txt');

								$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
								or die('Error connecting to MySQL server.');

								//$query = "Select * FROM stocks group by ticker order by price desc limit 10;";
								$query = "Select concat(fname, ' ',lname) as name, max(best_sim)* 100 as total FROM customer_new JOIN accounts_new using (cust_id) group by (cust_id) ORDER BY(total) DESC LIMIT 10;";
								$result = mysqli_query($conn, $query)
								or die(mysqli_error($conn));
								print "<pre>";
								printf( "%-20s \t %+10s", "Customer", "Best Simulation");
    							print "\n";
    							print "--------------------------------------\n";
								while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
									{
    									print "\n";
										//print "<option value=$row[0]> $row[1] </option>";
										printf( "%-20s \t %.2f%%", $row[name], $row[total]);

  									}
	    						print "</pre>";
								mysqli_free_result($result);
								mysqli_close($conn);
							?>

							<br>
							<br>

						</div>
						<div class="col-sm-3">
						Stocks to watch
							<?php
								print "<pre>";
								printf( "%-10s \t %+10s", "company", "ticker");
								print"\n";
    							print "---------------------------------\n";
    							print "\n";
							   printf( "%-20s \t %-5s\n", "Bank of America", "BAC");
							   printf( "%-20s \t %-5s\n", "Ford", "F");
							   printf( "%-20s \t %-5s\n", "General Electric", "GE");
							   printf( "%-20s \t %-5s\n", "Oracle", "ORCL");
							   printf( "%-20s \t %-5s\n", "AT&T", "T");
							   printf( "%-20s \t %-5s\n", "Visa", "V");
							   printf( "%-20s \t %-5s\n", "Coca-Cola", "KO");
							   printf( "%-20s \t %-5s\n", "Exxon Mobil", "XOM");
							   printf( "%-20s \t %-5s\n", "Fitbit", "FIT");
							   printf( "%-20s \t %-5s\n", "Walt Disney", "DIS");
	    						print "</pre>";
							?>

						</div>
				</div>
		  </div>
<br>
  </div>

	<br>

	<!-- <footer>
		<div class="container" id="footer_container">
			<div class="col-sm-4">
				<h3> Contact </h3>
				<hr/>
				<b>email:</b> YourScrewed@canntbebothered.com
				<br/>
				<b>office:</b> Somewhere in the USA 
				<br/>
				<br/>
			<div/>
		</div>
	</footer> -->
	<br/>

</body>
</html>
