<?php
	include('connectionData.txt');
	$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
	or die('Error connecting to MySQL server.');
	$forex= $_POST['forex'];
	$query = "SELECT * FROM forex_new WHERE name= ?;";
?>

<html>
<head>
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <div id="chart_div"></div>
  <script type="text/javascript">
  google.charts.load('current', {packages: ['corechart', 'line']});
  google.charts.setOnLoadCallback(drawBasic);
  data.addColumn('number', 'X');

    function drawBasic() {
 		var data = new google.visualization.DataTable();
		data.addColumn('date', 'X');
		<?php
		printf("data.addColumn('number', '%s');" ,$forex);
		?>

		data.addRows([
			<?php
			if(!($stmt = mysqli_prepare($conn, $query))){
				print "Prepare Failed";
			}
			if(!(mysqli_stmt_bind_param($stmt,"s", $forex))){
				print "Binding parameters failed";
			}
			if(!(mysqli_stmt_execute($stmt))){
				print "Execution failed";
			}
			$result = mysqli_stmt_get_result($stmt);
			$cnt = 0;
			while($row = mysqli_fetch_array($result, MYSQLI_BOTH))
  			{
				$dates = explode("-", $row[date]);
				if($cnt){
					printf( ",[new Date(%s,%s,%d), %f]", $dates[0],$dates[1] - 1,$dates[2], $row[price] );
				}else{
					printf( "[new Date(%s,%s,%d), %f]", $dates[0],$dates[1] - 1,$dates[2], $row[price] );
				}
				$cnt = $cnt +1;
  			}
			mysqli_free_result($result);
			mysqli_stmt_close($stmt);
			mysqli_close($conn);
			?>
		]);


	
      var options = {
		    hAxis: {
		    	   title: 'Date'
		    },
		    vAxis: {
		        title: 'Price'
		    },
        	chart: {
          		title: 'Stock Prices For the Last Two Weeks',
        	},
        	width: 1000,
        	height: 700
      	};


		var chart = new
		google.visualization.LineChart(document.getElementById('line_top_x'));
		chart.draw(data, options);
		}
		</script>

	<title>Stock Sesh</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>


	 h3{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
			color: grey;
		}

	 h2{
			font-weight:500;
			font-family: "Times New Roman", Times, serif;
		}

	 h1{
			font-weight:bold;
			font-variant: small-caps;
		}


		body{
			background-color: #eeeeee;
		}

	    #body-wrapper {
			background-color: #ffffff;
			margin-bottom: 0;
			margin: 0 auto;
			border-radius: 0; 
			width: 100%;
			position: auto;
			max-width: 1100px;
	    }



		footer{
			background-color: #ffffff;
      		margin-bottom: 0;
      		margin: 0 auto;
      		width: 100%;
      		position: auto;
      		max-width: 1100px;
			padding: 5px;
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}

		

  </style>
</head>



<body>


<br>
<div class="container" id="body-wrapper">
	<div class="container text-center">
		<div class="row content">
			<div class="col-sm-5 text-left">
  

	<p>
		<a href="home.php"><h1>Stock Sesh</h1></a>
		<h3>stock simulater</h3>
	</p>



			</div>
			<div class="col-sm-5 text-right">
			<br>
			<br>
			<br>
				<form action="tickerSearch.php" method="POST">
					Search stock ticker: <input type="text" name="ticker"> <input type="submit" value="Submit"> <input type="reset" value="Erase">
				</form>
			</div>
		</div>
	</div>
	<hr>

			<div class="container text-center">
						<div class="col-sm-2"></div>
					    <div class="col-sm-8 text-left">
						<h2>Stock Prices for ticker:<?php echo "$forex"; ?></h2>
						<h4>in dollars (USD)</h4>
						</div>
				<div class="row content">
					    <div class="col-sm-8 text-left" id="line_top_x">


							<br>
							<br>

						</div>

				</div>
		  </div>
<br>
  </div>

	<br>

  <div ></div>
	<!-- <footer>
		<div class="container" id="footer_container">
			<div class="col-sm-4">
				<h3> Contact </h3>
				<hr/>
				<b>email:</b> YourScrewed@canntbebothered.com
				<br/>
				<b>office:</b> Somewhere in the USA 
				<br/>
				<br/>
			<div/>
		</div>
	</footer> -->
	<br/>

</body>
</html>
