<html>
  <head>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  <div id="chart_div"></div>
  <script type="text/javascript">
  google.charts.load('current', {packages: ['corechart', 'line']});
  google.charts.setOnLoadCallback(drawBasic);

    function drawBasic() {
 		var data = new google.visualization.DataTable();
		data.addColumn('date', 'X');
		data.addColumn('number', 'Account Performance');

		data.addRows([
<?php
   include('connectionData.txt');
   
   $account = $_POST['account'];
   
   $conn = mysqli_connect($server, $user, $pass, $dbname, $port)
   or die('Error connecting to MySQL server.');
   
   $query = "SELECT * FROM positions_new
	     WHERE account_id = ?";

   $query_past_balance = "SELECT balance,best_sim FROM accounts_new
			  WHERE account_id = ?";

   $query_best_stock_price = "SELECT * FROM stock_new
			      WHERE investment_id = ? AND
			      stock_new.date <= DATE(?)
			      ORDER BY(stock_new.date) DESC LIMIT 1;";

   $update_best_sim = "UPDATE accounts_new
		       SET best_sim=?
		       WHERE account_id=?;";

   if(!($stmt = mysqli_prepare($conn, $query_past_balance))){
       print "Prepare Failed";
   }
   if(!(mysqli_stmt_bind_param($stmt,"s",$account))){
       print "Binding parameters failed";
   }
   if(!(mysqli_stmt_execute($stmt))){
       print "Execution failed";
   }
   $result = mysqli_stmt_get_result($stmt);   
   $row = mysqli_fetch_array($result, MYSQLI_BOTH);

   $current_balance = $row[balance];
   $best_sim = $row[best_sim];
   
   if(!($stmt = mysqli_prepare($conn, $query))){
       print "Prepare Failed";
   }
   if(!(mysqli_stmt_bind_param($stmt,"s",$account))){
       print "Binding parameters failed";
   }
   if(!(mysqli_stmt_execute($stmt))){
       print "Execution failed";
   }
   
   $total = $current_balance;
   $new_balance = $current_balance;
   
   $result = mysqli_stmt_get_result($stmt);
   $daily_balances = array();
   
   for ($x = 0; $x < 23; $x++){
       array_push($daily_balances, $current_balance);
   }
   
   $dates = array("2016-11-1","2016-11-2", "2016-11-3", "2016-11-4","2016-11-5","2016-11-6","2016-11-7","2016-11-8","2016-11-9","2016-11-10","2016-11-11","2016-11-12","2016-11-13","2016-11-14","2016-11-15","2016-11-16","2016-11-17","2016-11-18","2016-11-19","2016-11-20","2016-11-21","2016-11-22","2016-11-23");
   $array_sizes = 23;
   while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){
       $total = $total + ($row[price_bought] * $row[quantity]);
       
       for ($x = 0; $x < $array_sizes; $x++){
           if(!($stmt = mysqli_prepare($conn, $query_best_stock_price))){
               print "Prepare Failed";
           }
           if(!(mysqli_stmt_bind_param($stmt,"ss",$row[investment_id], $dates[$x]))){
               print "Binding parameters failed";
           }
           if(!(mysqli_stmt_execute($stmt))){
               print "Execution failed";
           }
           $result_sub = mysqli_stmt_get_result($stmt);
           $row_price = mysqli_fetch_array($result_sub, MYSQLI_BOTH);
           $daily_balances[$x] = $daily_balances[$x] + ($row_price[price] * $row[quantity]);
       
       }
   }
   
   foreach($daily_balances as &$tmp_balance){
       $tmp_balance = ($tmp_balance - $total) / $total;
   }

   $cnt = 0;
   for ($x = 0; $x < $array_sizes; $x++){
       $curr_date = explode("-", $dates[$x]);
   if($cnt){
	printf( ",[new Date(%s,%s,%d), %f]", $curr_date[0],$curr_date[1] - 1,$curr_date[2],  $daily_balances[$x]*100);
   }else{
	printf( "[new Date(%s,%s,%d), %f]", $curr_date[0],$curr_date[1] - 1,$curr_date[2],  $daily_balances[$x]*100);
   }
       $cnt = $cnt +1;
   }

   if($daily_balances[$array_sizes - 1] > $best_sim ){
       if(!($stmt = mysqli_prepare($conn, $update_best_sim))){
           print "Prepare Failed";
	   }
        if(!(mysqli_stmt_bind_param($stmt,"ss",$daily_balances[$array_sizes - 1],$account))){
	           print "Binding parameters failed";
          }
         if(!(mysqli_stmt_execute($stmt))){
            print "Execution failed";
           }
    }

   mysqli_free_result($result);
   mysqli_stmt_close($stmt);
   mysqli_close($conn);
?>
		]);


	
      var options = {
		    hAxis: {
		    	   title: 'Date'
		    },
		    vAxis: {
		        title: 'Percentage Gain'
		    },
        	chart: {
          		title: 'Stock Prices For the Last Two Weeks',
        	},
        	width: 1000,
        	height: 700
      	};


		var chart = new
		google.visualization.LineChart(document.getElementById('line_top_x'));
		chart.draw(data, options);
		}
		</script>
	<title>Stock Sesh</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>


	 h3{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
			color: grey;
		}

	 h2{
			font-weight:500;
			font-family: "Times New Roman", Times, serif;
		}

	 h1{
			font-weight:bold;
			font-variant: small-caps;
		}


		body{
			background-color: #eeeeee;
		}

	    #body-wrapper {
			background-color: #ffffff;
			margin-bottom: 0;
			margin: 0 auto;
			border-radius: 0; 
			width: 100%;
			position: auto;
			max-width: 1100px;
	    }



		footer{
			background-color: #ffffff;
      		margin-bottom: 0;
      		margin: 0 auto;
      		width: 100%;
      		position: auto;
      		max-width: 1100px;
			padding: 5px;
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}

		

  </style>
</head>



<body>


<br>
<div class="container" id="body-wrapper">
	<div class="container text-center">
		<div class="row content">
			<div class="col-sm-5 text-left">
				<p>
					<a href="home.php"><h1>Stock Sesh</h1></a>
					<h3>stock simulater</h3>
				</p>

			</div>
			<div class="col-sm-5 text-right">
				<br>
				<br>
				<br>


				<form action="tickerSearch.php" method="POST">
					<div class="form-inline form-space">
						<label for="ticker">Search stock ticker: </label>
						<input class="form-control" type="text" name="ticker">
						<input class="btn btn-success" type="submit" value="Submit">
						<input class="btn btn-danger" type="reset" value="Erase">
					</div>
				</form>



			</div>
		</div>
	</div>
	<hr>

			<div class="container text-center">
				<div class="col-sm-2"></div>
					<div class="col-sm-8 text-left">
					 	<h2>Account Performance for the Month of November</h2>
					    <h4>Your Monthly Gain: <?php printf("%.2f%%",$daily_balances[$array_sizes - 1] * 100); ?></h4>
					</div>
					<div class="row content">
						<div class="col-sm-8 text-left" id="line_top_x">

							<br>
							<br>

						</div>

					</div>
		  		</div>
				<br>
  			</div>

	<br>

</div>
	<!-- <footer>
		<div class="container" id="footer_container">
			<div class="col-sm-4">
				<h3> Contact </h3>
				<hr/>
				<b>email:</b> YourScrewed@canntbebothered.com
				<br/>
				<b>office:</b> Somewhere in the USA 
				<br/>
				<br/>
			<div/>
		</div>
	</footer> -->
	<br/>

</body>
</html>
