<!DOCTYPE html>
<html>
  <head>
    <title>Stock Sesh</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <style>

      h3{
      font-weight:500;
      font-variant: small-caps;
      font-family: "Times New Roman", Times, serif;
      color: grey;
      }

      h2{
      font-weight:500;
      font-variant: small-caps;
      font-family: "Times New Roman", Times, serif;
      }

      h1{
      font-weight:bold;
      font-variant: small-caps;
      }


      body{
      background-color: #eeeeee;
      }

      #body-wrapper {
      background-color: #ffffff;
      margin-bottom: 0;
      margin: 0 auto;
      border-radius: 0;
      width: 100%;
      position: auto;
      max-width: 1100px;
      }



      footer{
      background-color: #ffffff;
      margin-bottom: 0;
      margin: 0 auto;
      width: 100%;
      position: auto;
      max-width: 1100px;
      padding: 5px;
      font-weight:500;
      font-variant: small-caps;
      font-family: "Times New Roman", Times, serif;
      }




    </style>
  </head>
  <body>


    <br>
    <div class="container" id="body-wrapper">
      <div class="container text-center">
	<div class="row content">
	  <div class="col-sm-5 text-left">
	    <h1><a href="home.php">Stock Sesh</a></h1>
	    <h3>stock simulater</h3>
	  </div>
	  <div class="col-sm-5 text-right">
	    <br>
	    <br>
	    <form action="tickerSearch.php" method="POST">
	      Search stock ticker: <input type="text" name="ticker"> <input type="submit" value="Submit"> <input type="reset" value="Erase">
	    </form>
	  </div>
	</div>
      </div>
      <hr>

      <div class="container text-center">
	<div class="row content">
	  <div class="col-sm-8 text-left" id="div_top_accounts">
<?php
   include('connectionData.txt');
   
   $ticker = $_POST['ticker_buy'];
   $shares = $_POST['number_buy'];
   $account = $_POST['account_buy'];
   
   $conn = mysqli_connect($server, $user, $pass, $dbname, $port)
   or die('Error connecting to MySQL server.');
   
   $query = "SELECT * FROM stock_new
	     WHERE stock_new.ticker = ? AND
	     stock_new.date >= DATE('2016-11-01')
	     ORDER BY(stock_new.date) ASC
	     LIMIT 1;";

   $get_inv_id = "INSERT INTO positions_new  (account_id,investment_id, price_bought, date_bought,quantity)
		  VALUES (?,?,?,DATE('2016-11-01'),?);";

   $get_balance = "SELECT balance FROM accounts_new WHERE account_id =?";

   $update_balance = "UPDATE accounts_new SET balance = ? WHERE account_id = ?";
   
   
   if(!($stmt = mysqli_prepare($conn, $query))){
       print "Prepare Failed";
   }
   if(!(mysqli_stmt_bind_param($stmt,"s",$ticker))){
       print "Binding parameters failed";
   }
   if(!(mysqli_stmt_execute($stmt))){
       print "Execution failed";
   }
   $result = mysqli_stmt_get_result($stmt);
   while($row = mysqli_fetch_array($result, MYSQLI_BOTH)){

       $total_price = $row[price] * $shares;
   
   
       if(!($stmt = mysqli_prepare($conn, $get_balance))){
           print "Prepare Failed";
       }
       if(!(mysqli_stmt_bind_param($stmt,"s",$account))){
           print "Binding parameters failed";
       }   
       if(!(mysqli_stmt_execute($stmt))){
           print "Execution failed";
       }

       $balance_result = mysqli_stmt_get_result($stmt);
       $balance_row = mysqli_fetch_array($balance_result, MYSQLI_BOTH);
       $balance = $balance_row[balance];

       $diff = $balance - $total_price;

   if($diff >= 0){
       printf("Stocks Purchased Succesfully!");
       if(!($stmt = mysqli_prepare($conn, $update_balance))){
           print "Prepare Failed";
       }
       if(!(mysqli_stmt_bind_param($stmt,"ss",$diff,$account))){
            print "Binding parameters failed";
        }
        if(!(mysqli_stmt_execute($stmt))){
            print "Execution failed";
        }

        if(!($stmt = mysqli_prepare($conn, $get_inv_id))){
            print "Prepare Failed";
        }
        if(!(mysqli_stmt_bind_param($stmt,"ssss",$account,$row[investment_id],$row[price],$shares))){
           print "Binding parameters failed";
        }
        if(!(mysqli_stmt_execute($stmt))){
            print "Execution failed";
        }

     }else{
          printf("Insufficient Funds");
     }

   
   }
   mysqli_free_result($result);
   mysqli_stmt_close($stmt);
   mysqli_close($conn);
?>




							<br>
							<br>

	  </div>

	</div>
      </div>
      <br>
    </div>

    <br>

    <!-- <footer>
	 <div class="container" id="footer_container">
	   <div class="col-sm-4">
	     <h3> Contact </h3>
	     <hr/>
	     <b>email:</b> YourScrewed@canntbebothered.com
	     <br/>
	     <b>office:</b> Somewhere in the USA 
	     <br/>
	     <br/>
	     <div/>
	     </div>
    </footer> -->
    <br/>

  </body>
  </html>
