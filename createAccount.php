<?php
	include('connectionData.txt');
	$conn = mysqli_connect($server, $user, $pass, $dbname, $port)
	or die('Error connecting to MySQL server.');

	$cust_id = $_POST['cust_id'];
	$broker_id = $_POST['broker_select'];
	$type = $_POST['account_select'];
	$query = "insert into accounts_new (account_id, cust_id, broker_id, type, best_sim, balance) values (NULL, ?, ?, ?, NULL, 10000);";
	$cust_name = "Select fname, lname From customer_new WHERE cust_id=?;";

	?>

<html>
<head>
	<title>Stock Sesh</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>

	 h3{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
			color: grey;
		}

	 h2{
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}

	 h1{
			font-weight:bold;
			font-variant: small-caps;
		}


		body{
			background-color: #eeeeee;
		}

	    #body-wrapper {
			background-color: #ffffff;
			margin-bottom: 0;
			margin: 0 auto;
			border-radius: 0; 
			width: 100%;
			position: auto;
			max-width: 1100px;
	    }



		footer{
			background-color: #ffffff;
      		margin-bottom: 0;
      		margin: 0 auto;
      		width: 100%;
      		position: auto;
      		max-width: 1100px;
			padding: 5px;
			font-weight:500;
			font-variant: small-caps;
			font-family: "Times New Roman", Times, serif;
		}

		

  </style>
</head>



<body>


<br>
<div class="container" id="body-wrapper">
	<div class="container text-center">
		<div class="row content">
			<div class="col-sm-5 text-left">
  

	<p>
		<a href="home.php"><h1>Stock Sesh</h1></a>
		<h3>stock simulater</h3>
	</p>



			</div>
			<div class="col-sm-5 text-right">
			<br>
			<br>
			<br>
				<form action="tickerSearch.php" method="POST">
					<div class="form-inline form-space">
						<label for="ticker">Search stock ticker: </label>
						<input class="form-control" type="text" name="ticker">
						<input class="btn btn-success" type="submit" value="Submit">
						<input class="btn btn-danger" type="reset" value="Erase">
					</div>
				</form>

			</div>
		</div>
	</div>
	<hr>

			<div class="container text-center">
				<div class="row content">
					    <div class="col-sm-5 text-left" id="line_top_x">
							<?php
								if(!($stmt = mysqli_prepare($conn, $query))){
									print "Prepare Failed";
								}if(!(mysqli_stmt_bind_param($stmt,"sss", $cust_id, $broker_id, $type))){
									print "Binding parameters failed";
								}if(!(mysqli_stmt_execute($stmt))){
									print "Execution failed";
								}

								if(!($stmt2 = mysqli_prepare($conn, $cust_name))){
									print "Prepare Failed";
								}if(!(mysqli_stmt_bind_param($stmt2,"s", $cust_id))){
									print "Binding parameters failed";
								}if(!(mysqli_stmt_execute($stmt2))){
									print "Execution failed";
								}
								
   								$result = mysqli_stmt_get_result($stmt2);
   								$row = mysqli_fetch_array($result, MYSQLI_BOTH);
								if(mysqli_stmt_affected_rows($stmt) > 0){ 
									printf("Congratulations %s %s you've created a new account!",$row[fname], $row[lname]);
								}else{
									print"Unable to create account, not a valid customer ID";
								}
								

								mysqli_free_result($result);
								mysqli_stmt_close($stmt);
								mysqli_stmt_close($stmt2);
								mysqli_close($conn);
							?>

							<br>
							<br>

						</div>

				</div>
		  </div>
<br>
  </div>

	<br>

  <div ></div>
	<!-- <footer>
		<div class="container" id="footer_container">
			<div class="col-sm-4">
				<h3> Contact </h3>
				<hr/>
				<b>email:</b> YourScrewed@canntbebothered.com
				<br/>
				<b>office:</b> Somewhere in the USA 
				<br/>
				<br/>
			<div/>
		</div>
	</footer> -->
	<br/>

</body>
</html>
